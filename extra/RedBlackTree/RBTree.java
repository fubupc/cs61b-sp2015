class RBTree<K extends Comparable<K>, V> {
    class Node {
        private K key;
        private V val;
        private boolean color;

        private Node left;
        private Node right;


        public boolean isRed() {
            return color == RED;
        }

        public Node(K k, V v, boolean col) {
            key = k;
            val = v;
            left = null;
            right = null;
            color = col;
        }

        public void flipColors() {
            this.left.color = BLACK;
            this.right.color = BLACK;
            this.color = RED;
        }
    }

    public static final boolean RED = true;
    public static final boolean BLACK = false;

    private Node root;

    public boolean isRed(Node n) {
        if (n == null) return false;
        return n.color == RED;
    }

    public Node rotateLeft(Node node) {
        Node x = node.right;
        node.right = x.left;
        x.left = node;

        boolean tmp = node.color;
        node.color = x.color;
        x.color = tmp;

        return x;
    }

    public Node rotateRight(Node node) {
        Node x = node.left;
        node.left = x.right;
        x.right = node;

        boolean tmp = node.color;
        node.color = x.color;
        x.color = tmp;

        return x;
    }

    public void insert(K k, V v) {
        root = insert(root, k, v);
    }

    private Node insert(Node node, K k, V v) {
        if (node == null) {
            return new Node(k, v, RED);
        }

        int cmp = k.compareTo(node.key);

        if (cmp == 0) {
            node.val = v;
        } else if (cmp < 0) {
            node.left = insert(node.left, k, v);
        } else {
            node.right = insert(node.right, k, v);
        }

        if (isRed(node.right) && !isRed(node.left)) {
            node = rotateLeft(node);
        }

        if (isRed(node.left) && isRed(node.left.left)) {
            node = rotateRight(node);
        }

        if (isRed(node.left) && isRed(node.right)) {
            node.flipColors();
        }

        return node;
    }

    public void print() {
        printHelper(root);
    }

    public void printHelper(Node node) {
        if (node == null) return;

        System.out.print("(");
        printHelper(node.left);
        System.out.print(node.key);
        if (node.isRed()) {
            System.out.print("R");
        } else {
            System.out.print("B");
        }

        printHelper(node.right);
        System.out.print(")");
    }


    public static void main(String[] args) {
        RBTree<Integer, String> tree = new RBTree<Integer, String>();

        tree.insert(1, "A");
        tree.insert(2, "B");
        tree.insert(3, "C");
        tree.insert(4, "D");
        tree.insert(5, "E");
        tree.insert(6, "F");

        tree.print();
    }

}

