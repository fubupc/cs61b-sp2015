public class CommonAncestor {
    public static void main(String[] args) {

        BSTNode t = new BSTNode(4,
                new BSTNode(2,
                    new BSTNode(1),
                    new BSTNode(3)),
                new BSTNode(6,
                    new BSTNode(5),
                    new BSTNode(7)));


        System.out.print("1 and 3 's shortest common ancestor should be 2: ");
        if (commonAncestor(t, 1, 3) == null) {
            System.out.println("null");
        } else {
            System.out.println(commonAncestor(t, 1, 3).value);
        }

        System.out.print("1 and 5 's shortest common ancestor should be 4: ");
        if (commonAncestor(t, 1, 5) == null) {
            System.out.println("null");
        } else {
            System.out.println(commonAncestor(t, 1, 5).value);
        }

        System.out.print("2 and 7 's shortest common ancestor should be 4: ");
        if (commonAncestor(t, 2, 7) == null) {
            System.out.println("null");
        } else {
            System.out.println(commonAncestor(t, 2, 7).value);
        }

        System.out.print("3 and 8 's shortest common ancestor should be null: ");
        if (commonAncestor(t, 3, 8) == null) {
            System.out.println("null");
        } else {
            System.out.println(commonAncestor(t, 3, 8).value);
        }
    }

    /**
     * Returns the BSTNode that is the shortest common ancestor of n1 and n2. 
     * Assume that n1 < n2
     * Question: Can a node to be considered ancestor of itself?
     */
    public static BSTNode commonAncestor(BSTNode root, int n1, int n2) {
        if (root == null) {
            return null;
        }

        if (n1 > root.value) {
            return commonAncestor(root.right, n1, n2);
        } else if (n1 == root.value) {
            if (inTree(n2, root.right)) {
                return root;    // note: here assume a node can be considered ancestor of itself.
                // otherwise it should be root.parent return instead of root.
            } else {
                return null;
            }
        } else if (n2 > root.value) {
            if (inTree(n1, root.left) && inTree(n2, root.right)) {
                return root;
            } else {
                return null;
            }
        } else if (n2 == root.value) {
            if (inTree(n1, root.left)) {
                return root;    // same above
            } else {
                return null;
            }
        } else {
            return commonAncestor(root.left, n1, n2);
        }
    }


    public static boolean inTree(int n, BSTNode node) {
        if (node == null) {
            return false;
        }

        if (n == node.value) {
            return true;
        } else if (n < node.value) {
            return inTree(n, node.left);
        } else {
            return inTree(n, node.right);
        }
    }
}

class BSTNode {
    public BSTNode left, right;
    public int value;
    public BSTNode(int n) {
        value = n;
    }

    public BSTNode(int n, BSTNode left, BSTNode right) {
        this.value = n;
        this.left = left;
        this.right = right;
    }
}

