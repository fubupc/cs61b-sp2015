class Test {

    public void postOrder(Tree root){
        Stack s = new Stack();
        Tree current = root;
        Tree prevElement = null;
        while(!s.empty() || current !=null){
            if(current !=null){
                s.push(current);
                current = current.left;
            }else{
                current = s.lastElement();
                if(current.right == null || current.right == prevElement){
                    System.out.println(current.value);
                    prevElement = current;
                    s.pop();
                    current = null;
                }else{
                    current = current.right;
                }
            }
        }
    }
}
