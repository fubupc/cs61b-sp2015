public class BST {

    public static void main(String[] args) {

        int len = 10 - 1;
        int[] nums = new int[len];

        for (int i = 0; i < len; i++) {
            nums[i] = i + 1;
        }
        
        BSTNode tree = makeBST(nums);

        System.out.println("BST: \n" + tree);

        System.out.print("preRecur: ");
        preRecur(tree);
        System.out.print("\n");

        System.out.print("preIter: ");
        preIter(tree);
        System.out.print("\n");

        System.out.print("preStack1: ");
        preStack1(tree);
        System.out.print("\n");

        System.out.print("preStack2: ");
        preStack2(tree);
        System.out.print("\n");

        System.out.print("inRecur: ");
        inRecur(tree);
        System.out.print("\n");

        System.out.print("inIter: ");
        inIter(tree);
        System.out.print("\n");

        System.out.print("inStack(): ");
        inStack(tree);
        System.out.print("\n");

        System.out.print("postRecur: ");
        postRecur(tree);
        System.out.print("\n");

        System.out.print("postIter: ");
        postIter(tree);
        System.out.print("\n");

        System.out.print("postStack(): ");
        postStack(tree);
        System.out.print("\n");

        System.out.print("postStack2(): ");
        postStack2(tree);
        System.out.print("\n");

        System.out.print("traversal(): ");
        traversal(tree);
        System.out.print("\n");
    }

    public static BSTNode makeBST(int[] nums) {
        if (nums.length == 0) {
            return null;
        } else if (nums.length == 1) {
            return new BSTNode(nums[0]);
        } else {
            int mid = nums.length / 2;

            BSTNode node = new BSTNode(nums[mid]);

            node.setLeft(makeBST(slice(nums, 0, mid)));

            if (mid < nums.length - 1) {
                node.setRight(makeBST(slice(nums, mid + 1, nums.length)));
            }

            return node;
        }
    }

    public static int[] slice(int[] arr, int start, int end) {
        int[] sliced = new int[end - start];

        for (int i = start; i < end; i++) {
            sliced[i - start] = arr[i];
        }

        return sliced;
    }

    public static void preRecur(BSTNode node) {
        System.out.print(node.value + "  ");

        if (node.left != null) {
            preRecur(node.left);
        }
        
        if (node.right != null) {
            preRecur(node.right);
        }
    }

    public static void inRecur(BSTNode node) {

        if (node.left != null) {
            inRecur(node.left);
        }
        
        System.out.print(node.value + "  ");

        if (node.right != null) {
            inRecur(node.right);
        }
    }

    public static void postRecur(BSTNode node) {

        if (node.left != null) {
            postRecur(node.left);
        }
        
        if (node.right != null) {
            postRecur(node.right);
        }

        System.out.print(node.value + "  ");
    }

    /**
     * Iterative solution WITHOUT stack.
     */
    public static void preIter(BSTNode node) {
        BSTNode current = node;
        BSTNode prev = null;

        while (current != null) {

            if ((prev == null) || (prev == current.parent)) {
                System.out.print(current.value + "  ");

                prev = current;

                if (current.left != null) {
                    current = current.left;
                } else if (current.right != null) {
                    current = current.right;
                } else {
                    current = current.parent;
                }
            } else if (prev.parent == current) {

                if (current.left == prev) {
                    prev = current;
                    current = current.right;
                } else {
                    prev = current;
                    current = current.parent;
                }

            }

        }
    }

    public static void inIter(BSTNode node) {
        BSTNode current = node;
        BSTNode prev = null;

        while (current != null) {

            if ((prev == null) || (prev == current.parent)) {

                prev = current;

                if (current.left != null) {
                    current = current.left;
                } else if (current.right != null) {
                    System.out.print(current.value + "  ");
                    current = current.right;
                } else {
                    System.out.print(current.value + "  ");
                    current = current.parent;
                }
            } else if (prev.parent == current) {

                if (current.left == prev) {
                    System.out.print(current.value + "  ");
                    prev = current;
                    current = current.right;
                } else {
                    prev = current;
                    current = current.parent;
                }

            }

        }
    }

    public static void postIter(BSTNode node) {
        BSTNode current = node;
        BSTNode prev = null;

        while (current != null) {

            if ((prev == null) || (prev == current.parent)) {

                prev = current;

                if (current.left != null) {
                    current = current.left;
                } else if (current.right != null) {
                    current = current.right;
                } else {
                    System.out.print(current.value + "  ");
                    current = current.parent;
                }
            } else if (prev.parent == current) {

                if (current.left == prev) {
                    prev = current;
                    current = current.right;
                } else {
                    System.out.print(current.value + "  ");
                    prev = current;
                    current = current.parent;
                }

            }

        }
    }

    /**
     * Iterative solution WITH stack.
     */
    public static void preStack1(BSTNode node) {
        BSTNode current = node;
        boolean done = false;
        Stack<BSTNode> st = new Stack<BSTNode>();

        while (!done) {
            if (current != null) {
                System.out.print(current.value + "  ");
                st.push(current);
                current = current.left;
            } else {
                if (st.empty()) {
                    done = true;
                } else {
                    current = st.pop();
                    current = current.right;
                }
            }
        }

    }


    public static void preStack2(BSTNode node) {
        if (node == null) {
            return;
        }

        BSTNode current;
        Stack<BSTNode> st = new Stack<BSTNode>();
        st.push(node);

        while (!st.empty()) {

            current = st.pop();

            System.out.print(current.value + "  ");

            if (current.right != null) {
                st.push(current.right);
            }

            if (current.left != null) {
                st.push(current.left);
            }
        }
    }

    public static void inStack(BSTNode node) {
        BSTNode current = node;
        boolean done = false;
        Stack<BSTNode> st = new Stack<BSTNode>();

        while (!done) {
            if (current != null) {
                st.push(current);
                current = current.left;
            } else {
                if (st.empty()) {
                    done = true;
                } else {
                    current = st.pop();
                    System.out.print(current.value + "  ");
                    current = current.right;
                }
            }
        }
    }

    public static void postStack(BSTNode node) {
        BSTNode current = node;
        BSTNode prev = null;
        Stack<BSTNode> st = new Stack<BSTNode>();

        while (!st.empty() || current != null) {
            if (prev == null || prev.left == current || prev.right == current) {
                if (current.left != null) {
                    st.push(current);
                    prev = current;
                    current = current.left;
                } else if (current.right != null) {
                    st.push(current);
                    prev = current;
                    current = current.right;
                } else {
                    System.out.print(current.value + "  ");
                    prev = current;
                    current = st.pop();
                }
            } else if (current.left == prev) {
                if (current.right != null) {
                    st.push(current);
                    prev = current;
                    current = current.right;
                } else {
                    System.out.print(current.value + "  ");
                    prev = current;
                    current = st.pop();
                }
            } else if (current.right == prev) {
                System.out.print(current.value + "  ");
                prev = current;
                if (!st.empty()) {
                    current = st.pop();
                } else {
                    current = null;
                }
            }
        }

    }

    public static void postStack2(BSTNode node) {
        BSTNode current;

        Stack<BSTNode> s1 = new Stack<BSTNode>();
        Stack<BSTNode> s2 = new Stack<BSTNode>();

        s1.push(node);

        while (!s1.empty()) {
            current = s1.top();

            if (current == s2.top()) {
                System.out.print(current.value + "  ");
                s1.pop();
                s2.pop();
            } else {
                if (current.right == null && current.left == null) {
                    System.out.print(current.value + "  ");
                    s1.pop();
                } else {
                    s2.push(current);

                    if (current.right != null) {
                        s1.push(current.right);
                    }

                    if (current.left != null) {
                        s1.push(current.left);
                    }
                }
            }
        }
    }

    /** Generic method: emulate stack changes of recurvice call for all 3 traversal process.
     */

    public static void traversal2(BSTNode node) {
        BSTNode current, prev = null;
        Stack<BSTNode> st = new Stack<BSTNode>();
        st.push(node);

        while (!st.empty()) {
            current = st.top();
            // System.out.print(current.value + "  ");  // pre-order

            if (prev == null || prev.left == current || prev.right == current) {
                if (current.left != null) {
                    st.push(current.left);
                } else if (current.right != null) {
                    st.push(current.right);
                }
            } else if (current.left == prev) {
                // System.out.print(current.value + "  ");  // in-order
                if (current.right != null) {
                    st.push(current.right);
                }
            } else {
                // System.out.print(current.value + "  ");  // post-order
                current = st.pop();
            }

            prev = current;
        }
    }

    public static void traversal(BSTNode node) {

        Stack<BSTNode> s = new Stack<BSTNode>(); 
        BSTNode current = node; 
        BSTNode prev = null;

        while (current != null || !s.empty()) {
            if (current != null){
                s.push(current);
                //visit,先序
                current = current.left;
            } else {
                current = s.top();
                //visit,中序
                if (current.right == prev || current.right == null) {
                    prev = s.pop();
                    current = null;
                    //visit,后序
                } else {
                    System.out.print(current.value + "  ");
                    current = current.right;
                    prev = null;
                }
            }
        }
    }


}


class Info {
    public BSTNode node;
    public int direction;

    public static final int DOWNLEFT = -1;
    public static final int DOWNRIGHT = 1; 
    public static final int UP = 0; 


    public Info(BSTNode n, int d) {
        node = n;
        direction = d;
    }
}


class BSTNode {
    public BSTNode left, right, parent;
    public int value;

    public BSTNode(int n) {
        value = n;
        left = null;
        right = null;
        parent = null;
    }

    public String toString() {
        return vert(this, 0);
    }

    public boolean isLeaf() {
        return (left == null) && (right == null);
    }

    public void setLeft(BSTNode node) {
        left = node;
        node.parent = this;
    }

    public void setRight(BSTNode node) {
        right = node;
        node.parent = this;
    }

    public static String vert(BSTNode n, int depth) {
        String prefix = "";
        String out = "";

        for (int i = 0; i < depth; i++) {
            prefix += "    ";
        }

        if (n == null) {
            return prefix + "\n";
        }

        if (n.isLeaf()) {
            return prefix + n.value + "\n";
        } else {
            out += vert(n.right, depth + 1);
            out += prefix + n.value + "\n";
            out += vert(n.left, depth + 1);

            return out;
        }
    }
}
