class Stack<T> {

    private int size;
    private StackNode top;

    public Stack() {
        size = 0;
        top = null;
    }

    public void push(T item) {
        top = new StackNode(item, top);
        size++;
    }

    public T pop() {
        if (top == null) {
            throw new RuntimeException("Empty stack!");
        }

        StackNode n = top;
        top = top.next;
        n.next = null;
        size--;

        return (T) n.item;
    }

    public boolean empty() {
        return size == 0;
    }

    public T top() {
        if (top == null) {
            return null;
        } else {
            return (T) top.item;
        }
    }
}


class StackNode {
    Object item;
    StackNode next;

    public StackNode(Object item1, StackNode next1) {
        item = item1;
        next = next1;
    }
}

