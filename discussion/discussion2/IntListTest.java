class IntListTest {

    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 4, 5, 6};

        IntList orig = IntList.list(nums);
        System.out.println(orig);

        IntList nonDestructive = IntList.reverseNonDestructive(orig);
        System.out.println("** After reverseNonDestructive(orig) **");
        System.out.println("orig: " + orig);
        System.out.println("reverse non-destructive: " + nonDestructive);

        IntList destructive = IntList.reverseDestructive(orig);
        System.out.println("** After reverseDestructive(orig) **");
        System.out.println("orig: " + orig);
        System.out.println("reverse destructive: " + destructive);

    }

}
            
