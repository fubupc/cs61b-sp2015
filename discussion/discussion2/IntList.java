import java.util.Formatter;

class IntList {
    int head;
    IntList tail;

    public IntList(int head0, IntList tail0) {
        head = head0;
        tail = tail0;
    }

    public static IntList list(int[] nums) {
        if (nums.length == 0) {
            return null;
        }

        IntList result = new IntList(nums[0], null);
        IntList ptr = result;

        for (int i = 1; i < nums.length; i++) {
            ptr.tail = new IntList(nums[i], null);
            ptr = ptr.tail;
        }

        return result;
    }

    public static IntList reverseNonDestructive(IntList L) {
        IntList x = null;

        while (L != null) {
            x = new IntList(L.head, x);
            L = L.tail;
        }

        return x;
    }

    public static IntList reverseDestructive(IntList L) {
        IntList prev = null;
        IntList current = L;

        while (current != null) {
            IntList next = current.tail;
            current.tail = prev;
            prev = current;
            current = next;
        }

        return prev;
    }

    private int detectCycles(IntList A) {
        IntList tortoise = A;
        IntList hare = A;

        if (A == null)
            return 0;

        int cnt = 0;


        while (true) {
            cnt++;
            if (hare.tail != null)
                hare = hare.tail.tail;
            else
                return 0;

            tortoise = tortoise.tail;

            if (tortoise == null || hare == null)
                return 0;

            if (hare == tortoise)
                return cnt;
        }
    }

    @Override
    /** Outputs the IntList as a String. You are not expected to read
     * or understand this method. */
    public String toString() {
        Formatter out = new Formatter();
        String sep;
        sep = "(";
        int cycleLocation = detectCycles(this);
        int cnt = 0;

        for (IntList p = this; p != null; p = p.tail) {
            out.format("%s%d", sep, p.head);
            sep = ", ";

            cnt++;
            if ((cnt > cycleLocation) && (cycleLocation > 0)) {
                out.format("... (cycle exists) ...");
                break;
            }
        }
        out.format(")");
        return out.toString();
    }

}
