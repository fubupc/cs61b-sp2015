class SNode {
    Integer val;
    SNode prev;

    public SNode(Integer v, SNode p) {
        val = v;
        prev = p;
    }

    public SNode(Integer v) {
        this(v, null);
    }

}


class BadIntStack {

    private SNode top;

    public BadIntStack() {
        top = null;
    }

    public boolean isEmpty() {
        return top == null;
    }

    public void push(Integer v) {
        top = new SNode(v, top);
    }

    public Integer pop() {
        if (isEmpty()) {
            throw new RuntimeException("Can not pop because stack is empty!");
        }

        Integer ans = top.val;

        top = top.prev;

        return ans;
    }

    public Integer peak() {
        if (isEmpty()) {
            throw new RuntimeException("Can not pop because stack is empty!");
        }

        return top.val;
    }
}
