class RockTest {
    public static void main(String[] args) {
        Rock r1 = new Rock(1);
        Rock r2 = new Rock(2);
        Rock r3 = new Rock(3);

        Rock[] rox1 = {r1, r2};
        Rock[] rox2 = {r2, r3};

        Rocks rs = new Rocks(rox1);

        rs.rocks[0] = r3;
        System.out.println(rs.rocks[0].weight);

        // rs.rocks = rox2; // error.
        rox1[0] = r2;
        System.out.println(rs.rocks[0].weight);
    }


}

class Rock {
    public final int weight;
    public Rock(int w) { weight = w; }
}

class Rocks {
    public final Rock[] rocks;

    public Rocks(Rock[] rox) { rocks = rox; }
}
