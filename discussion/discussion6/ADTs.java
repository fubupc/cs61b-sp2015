List {
    insert(item, position); // inserts item into the list at the position
    get(position); // returns the item in the list at the position
    size(); // returns the number of items in the list
}
Set {
    add(item); // puts item in the set. Does not add duplicates
    contains(item); // returns whether or not the item is in the set
    items(); // returns a List of all items in some arbitrary order
}
Stack {
    push(item); // puts item onto the stack
    pop(); // removes and returns the most recently put item
    isEmpty(); // returns whether the stack is empty
}
Queue {
    enqueue(item); // puts item into the queue
    dequeue(); // removes and returns the least recently put item
    isEmpty(); // returns whether the queue is empty
}
PriorityQueue {
    enqueue(item, priority); // puts item into the queue with a priority
    dequeue(); // removes and returns the item with highest priority
    peek(); // returns but does not remove the item with highest priority
}
Map { // like a dictionary from python
    put(key, value); /* puts key into the map and associates it with the
                        given value. If key is already in the map, replaces its existing
                        value with the given value */
    get(key); // returns value associated with key
    keys(); // returns a List of all keys in some arbitrary order
}
