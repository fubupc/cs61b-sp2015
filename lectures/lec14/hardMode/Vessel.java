/** Your job: Convert this into a generic Vessel so that TestGoalOne works.
 *  @author Josh Hug
 */

public class Vessel<T> {
    T occupant;

    public void put(T x) {
        occupant = x;
    }

    public T peek() {
        return occupant;
    }
} 
