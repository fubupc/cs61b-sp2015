class TreeDiameter {
    public static void diameter(BST<K> tree) {
        if (tree == null || tree.isLeaf()) {
            return 0;
        } else {
            // if longest path cross tree's root node, then diameter = height(left) + height(right) + 2
            int diameterCrossRoot = BST.height(tree.left) +  BST.height(tree.right) + 2;

            return Max.max(Max.max(diameterCrossRoot, diameter(tree.left)), diameter(tree.right));
        }
    }

    public static int max(int a, int b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }

}


class BST<K extends Comparable> {
    private K key;
    private BST<K> left;
    private BST<K> right;

    public BST(K k, BST<K> l, BST<K> r) {
        key = k;
        left = l;
        right = r;
    }

    public BST(K k) {
        this(k, null, null);
    }

    public isLeaf() {
        return left == null && right == null;
    }

    public static int height(BST tree) {
        if (tree == null) return -1;

        int leftHeight = height(tree.left);
        int rightHeight = height(tree.left);

        if (leftHeight > rightHeight) {
            return leftHeight + 1;
        } else {
            return rightHeight + 1;
        }
    }

}

