public class MergeRight {


    public static void main(String[] args) {
        IntTree T = new IntTree(-1, 
                new IntTree(12,
                    new IntTree(3,
                        null,
                        new IntTree(10,
                            new IntTree(8),
                            null)),
                    new IntTree(16,
                        new IntTree(14),
                        new IntTree(27))),
                null);

        IntTree L = new IntTree(-1,
                new IntTree(1,
                    null,
                    new IntTree(11,
                        null,
                        new IntTree(26))));

        System.out.println("T: \n" + inOrder(T));
        System.out.println("After merge: \n" + inOrder(mergeRight(T, L)));
    }

    public static IntTree removeFirst(IntTree L) {
        IntTree first = L.left;
        L.left = first.right;
        first.right = null;

        return first;
    }

    public static IntTree mergeRight(IntTree T, IntTree L) {

        IntTree current = T.left;

        Stack stack = new Stack();

        while (L.left != null) {
            IntTree first = L.left;

            if (first.data < current.data) {
                if (current.left == null) {
                    current.left = removeFirst(L);
                } else {
                    stack.push(current);
                    current = current.left;
                }
            } else if (first.data == current.data) {
                removeFirst(L);
            } else {
                if (current.right == null) {
                    if (stack.isEmpty()) {
                        current.right = L.left;
                        L.left = null;
                    } else {
                        if (first.data < ((IntTree) stack.top()).data) {
                            current.right = removeFirst(L);
                        } else {
                            current = (IntTree) stack.pop();
                        }
                    }
                } else {
                    current = current.right;
                }
            }
        }

        return T;
    }


    public static String inOrder(IntTree T) {
        return inHelper(T.left, 0);
    }

    public static String inHelper(IntTree t, int depth) {
        String prefix = "";
        String out = "";

        for (int i = 0; i < depth; i++) {
            prefix += "    ";
        }

        if (t == null) {
            out = prefix + "\n";
        } else {
            if ((t.left == null) && (t.right == null)) {
                out += prefix + t.data + "\n";
            } else {
                out += inHelper(t.right, depth + 1);
                out += prefix + t.data + "\n";
                out += inHelper(t.left, depth + 1);
            }
        }

        return out;
    }

}


class IntTree {
    public IntTree (int data) {
        this.data = data; this.left = null; this.right = null;
    }

    public IntTree (int data, IntTree left) {
        this.data = data; this.left = left; this.right = null;
    }

    public IntTree (int data, IntTree left, IntTree right) {
        this.data = data; this.left = left; this.right = right;
    }
    public final int data;
    public IntTree left, right;
}


class Stack {

    StackNode head;

    public Stack() {
        head = null;
    }

    public Object pop() {
       if (head == null) {
          return null;
       } else {
          StackNode o = head;
          head = head.next;
          return o.item;
       }
    }

    public boolean isEmpty() {
        return head == null;
    }

    public Object top() {
        if (head == null) {
            return null;
        } else {
            return head.item;
        }
    }

    public void push(Object item) {
        StackNode n = new StackNode(item);
        n.next = head;
        head = n;
    }
}


class StackNode {
    public Object item;
    public StackNode next;

    public StackNode(Object o) {
        item = o;
        next = null;
    }
}
