class Planet {

    double x;
    double y;
    double xVelocity;
    double yVelocity;
    double mass;

    double xNetForce;
    double yNetForce;
    double xAccel;
    double yAccel;

    String img;
    static final double G = 6.67e-11;

    static final int canvasWidth = 512;
    static final int canvasHeight = 512;

    public Planet(double xx, double yy, double xv, double yv, double m, String image) {
        x = xx;
        y = yy;
        xVelocity = xv;
        yVelocity = yv;
        mass = m;
        img = image;
        xNetForce = 0.0;
        yNetForce = 0.0;
        xAccel = 0.0;
        yAccel = 0.0;
    }

    public double calcDistance(Planet other) {
        double dx = other.x - x;
        double dy = other.y - y;
        return sqrt(dx * dx + dy * dy);
    }

    public double calcPairwiseForce(Planet other) {
        //double r = calcDistance(other);
        //return G * mass * other.mass / (r * r);
        double dx = other.x - x;
        double dy = other.y - y;
        return G * mass * other.mass / (dx * dx + dy * dy);
    }

    public double calcPairwiseForceX(Planet other) {
        return calcPairwiseForce(other) * (other.x - x) / calcDistance(other);
    }

    public double calcPairwiseForceY(Planet other) {
        return calcPairwiseForce(other) * (other.y - y) / calcDistance(other);
    }

    public void setNetForce(Planet[] planets) {
        xNetForce = 0;
        yNetForce = 0;

        for (Planet p: planets) {
            if (p != this)  {
                xNetForce += calcPairwiseForceX(p);
                yNetForce += calcPairwiseForceY(p);
            }
        }
    }

    public void update(double dt) {
        xAccel = xNetForce / mass;
        yAccel = yNetForce / mass;

        xVelocity += xAccel * dt;
        yVelocity += yAccel * dt;

        x += xVelocity * dt;
        y += yVelocity * dt;
    }

    public void draw() {
        StdDraw.picture(x, y, "images/" + img);
    }


    /** Helper method
     */

    public static double sqrt(double x) {
        double guess = 1.0;

        while (!closeEnough(guess, improveGuess(guess, x))) {
            guess = improveGuess(guess, x);
        } 

        return guess;
    }

    public static double improveGuess(double guess, double x) {
        return (guess + x / guess) / 2;
    }

    public static boolean closeEnough(double oldGuess, double newGuess) {
        return Math.abs(newGuess - oldGuess) / oldGuess < 0.0001;
    }

}
