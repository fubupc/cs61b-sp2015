class NBody {
    public static void main(String[] args) {
        double T = Double.parseDouble(args[0]);
        double dt = Double.parseDouble(args[1]);
        String filename  = args[2];

        In in = new In(filename);

        int num = in.readInt();
        double radius = in.readDouble();

        Planet[] planets = new Planet[num];

        StdDraw.setScale(-radius, radius);
        StdDraw.picture(0, 0, "images/starfield.jpg");

        for (int i = 0; i < num; i++) {
            planets[i] = getPlanet(in);
            planets[i].draw();
        }

        for (double t = 0; t < T; t += dt) {
            StdDraw.picture(0, 0, "images/starfield.jpg");

            for (Planet p : planets) {
                p.setNetForce(planets);
                p.update(dt);
                //System.out.println(p.img + " x: " + p.x + ", y: " + p.y);
                if (p.img == "sun.gif") {
                    System.out.println("sum x: " + p.x + ", y: " + p.y);
                }
                p.draw();
            }

            StdDraw.show(10);
        }


    }

    public static Planet getPlanet(In in) {
        double x = in.readDouble();
        double y = in.readDouble();
        double xVelocity = in.readDouble();
        double yVelocity = in.readDouble();
        double mass = in.readDouble();
        String image = in.readString();

        return new Planet(x, y, xVelocity, yVelocity, mass, image);
    }

}
