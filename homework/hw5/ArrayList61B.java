import java.util.AbstractList;

class ArrayList61B<E> extends AbstractList<E>  {
    protected E[] arr;
    protected int size;

    public ArrayList61B(int initialCapacity) {
        if (initialCapacity < 1) {
            throw new IllegalArgumentException();
        }

        arr = (E[]) new Object[initialCapacity];
        size = 0;
    }

    public ArrayList61B() {
        arr = (E[]) new Object[1];
        size = 0;
    }

    public E get(int i) {
        if (i < 0 || i >= size) {
            throw new IllegalArgumentException();
        }

        return arr[i];
    }

    public boolean add(E item) {
        if (size == arr.length) {
            resize();
        }

        arr[size] = item;
        size++;

        return true;
    }

    protected void resize() {
        E[] newArr = (E[]) new Object[2 * size];

        for (int i = 0; i < size; i++) {
            newArr[i] = arr[i];
        }

        arr = newArr;
    }

    public int size() {
        return size;
    }

}

