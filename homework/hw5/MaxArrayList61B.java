import java.util.AbstractList;

class MaxArrayList61B<E extends Comparable<E>> extends ArrayList61B<E>  {
    public boolean add(E item) {

        for (int i = 0; i < size(); i++) {
            E e = get(i);

            if (item.compareTo(e) <= 0) {
                return false;
            }
        } 

        return super.add(item);
    }

}

