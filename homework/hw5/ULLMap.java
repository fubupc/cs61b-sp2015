import java.util.Set; /* java.util.Set needed only for challenge problem. */
import java.util.Iterator;

/** A data structure that uses a linked list to store pairs of keys and values.
 *  Any key must appear at most once in the dictionary, but values may appear multiple
 *  times. Supports get(key), put(key, value), and contains(key) methods. The value
 *  associated to a key is the value in the last call to put with that key. 
 *
 *  For simplicity, you may assume that nobody ever inserts a null key or value
 *  into your map.
 */ 
public class ULLMap<K, V> implements Map61B<K, V>, Iterable<K> { //FIX ME
    /** Keys and values are stored in a linked list of Entry objects.
      * This variable stores the first pair in this linked list. You may
      * point this at a sentinel node, or use it as a the actual front item
      * of the linked list. 
      */
    private Entry front;

    @Override
    public V get(K key) { //FIX ME
        if (front == null) {
            return null;
        }

        Entry e = front.get(key);
        if (e != null) {
            return e.val;
        } else {
            return null;
        }
    }

    @Override
    public void put(K key, V val) { //FIX ME
        if (front != null) {
            Entry e = front.get(key);
            if (e != null) {
                e.val = val;
                return;
            }
        }
        front = new Entry(key, val, front);
    }

    @Override
    public boolean containsKey(K key) { //FIX ME
        return get(key) != null;
    }

    @Override
    public int size() {
        int size = 0;
        
        Entry current = front;

        while (current != null) {
            size++;
            current = current.next;
        }

        return size; // FIX ME (you can add extra instance variables if you want)
    }

    @Override
    public void clear() {
        front = null;
    }


    public static <V, K> ULLMap<V, K> reverse(ULLMap<K, V> m) {
        ULLMap<V, K> r = new ULLMap<V, K>();

        for (K key : m) {
            r.put(m.get(key), key);
        }

        return r;
    }

    public ULLMapIter iterator() {
        return new ULLMapIter();
    }


    /** Represents one node in the linked list that stores the key-value pairs
     *  in the dictionary. */
    private class Entry {
    
        /** Stores KEY as the key in this key-value pair, VAL as the value, and
         *  NEXT as the next node in the linked list. */
        public Entry(K k, V v, Entry n) { //FIX ME
            key = k;
            val = v;
            next = n;
        }

        /** Returns the Entry in this linked list of key-value pairs whose key
         *  is equal to KEY, or null if no such Entry exists. */
        public Entry get(K k) { //FIX ME
            Entry current = this;

            while (current != null) {
                if (k.equals(current.key)) {
                    break;
                }

                current = current.next;
            }

            return current; //FIX ME
        }

        /** Stores the key of the key-value pair of this node in the list. */
        private K key; //FIX ME
        /** Stores the value of the key-value pair of this node in the list. */
        private V val; //FIX ME
        /** Stores the next Entry in the linked list. */
        private Entry next;
    
    }

    private class ULLMapIter implements Iterator<K> {
        private Entry current = front;

        public boolean hasNext() {
            return current != null;
        }

        public K next() {
            Entry e = current;
            if (e == null) {
                return null;
            }
            current = current.next;
            return e.key;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    /* Methods below are all challenge problems. Will not be graded in any way. 
     * Autograder will not test these. */
    @Override
    public V remove(K key) { //FIX ME SO I COMPILE
        throw new UnsupportedOperationException();
    }

    @Override
    public V remove(K key, V value) { //FIX ME SO I COMPILE
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<K> keySet() { //FIX ME SO I COMPILE
        throw new UnsupportedOperationException();
    }


}
