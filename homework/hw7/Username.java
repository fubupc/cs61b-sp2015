public class Username {

    // Potentially useless note: (int) '0' == 48, (int) 'a' == 97

    // Instance Variables (remember, they should be private!)
    // YOUR CODE HERE
    private String name;

    public static char randomChar() {
        int num = (int) (Math.random() * 36);

        if (num < 10) {
            return (char) (num + (int) '0');
        } else  {
            return (char) (num - 10 + (int) 'a');
        }
    }

    public static boolean validChar(char c) {
        if ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z'))
            return true;
        else
            return false;
    }

    public Username() {
        name = "";

        int len = (int) (Math.random() * 2) + 2;

        for (int i = 0; i < len; i++) {
            name += randomChar();
        }
    }

    public Username(String reqName) {
        if (reqName == null) {
            throw new NullPointerException("Requested username is null!");
        }

        if (reqName.length() < 2 || reqName.length() > 3) {
            throw new IllegalArgumentException("Username length is not valid. (should be 2-3 characters)");
        }

        for (int i = 0; i < reqName.length(); i++) {
            if (!validChar(reqName.charAt(i))) {
                throw new IllegalArgumentException("Username has invalid character. (at index " + i + " of '" + reqName + "')");
            }
        }
        
        name = reqName.toLowerCase();
    }

    @Override
    public boolean equals(Object o) {
        // YOUR CODE HERE
        if (o != null && o instanceof Username) {
            return this.name.equals(((Username) o).name);
        }

        return false;
    }

    /* power of base. assuming power is positive. */
    public static int power(int base, int power) {
        int result = 1;

        while (power > 0) {
            result = result * base;
            power--;
        }

        return result;
    }

    @Override
    public int hashCode() { 
        int hash = 0;

        for (int i = 0; i < name.length(); i++) {
            hash += charToInt(name.charAt(i)) * power(36, i);
        }

        return hash;
    }

    /* Convert char into an int (0 - 61).
     * e.g. '0' -> 0, '9' -> 9, 'a' -> 10, 'z' -> 35, 'A' -> 36', 'Z' -> 61.
     */
    public static int charToInt(char c) {
        if (c >= '0' && c <= '9') {
            return c - '0';
        } else { // c >= 'a' && c <= 'z'
            return c - 'a' + 10;
        }
    }


    public static void main(String[] args) {
        // You can put some simple testing here.
        Username u;

        for (int i = 0; i < 100; i++) {
            u = new Username();
            System.out.println(u.name + ": " + u.hashCode());
        }

        // System.out.println(new Username("12345")); // error
        // System.out.println(new Username("!ox")); // error
        System.out.println(new Username("oox"));
    }
}
