import java.util.Set;

class BSTMap<K extends Comparable<K>, V> implements Map61B<K, V> {
    private BSTNode<K, V> root;
    private int size;

    public BSTMap() {
        root = null;
        size = 0;
    }

    public int size() {
        return size;
    }

    public void clear() {
        root = null;
    }

    public V get(K key) {
        return BSTNode.get(root, key);
    }

    public void put(K key, V val) {
        root = BSTNode.insert(root, key, val);
        size++;
    }

    public boolean containsKey(K key) {
        return get(key) != null;
    }

    /* TODO: to be improved. The key is searched two times. */
    public V remove(K key) {
        V val = get(key);
        if (val == null) return null;

        root = BSTNode.remove(root, key);

        return val;
    }

    /* TODO: to be improved */
    public V remove(K key, V value) {
        V val = get(key);

        if (val == null || !val.equals(value)) return null;

        root = BSTNode.remove(root, key);

        return val;
    }

    public Set<K> keySet() {
        throw new UnsupportedOperationException();
    }

    public void printInOrder() {
        printHelper(root);
    }

    public static void printHelper(BSTNode<? extends Comparable, ?> node) {
        if (node == null) return;

        printHelper(node.left);
        System.out.print(node.val + " ");
        printHelper(node.right);
    }
}


class BSTNode<K extends Comparable<K>, V> {

    K key;
    V val;
    BSTNode<K, V> left;
    BSTNode<K, V> right;

    public BSTNode(K k, V v, BSTNode<K, V> l, BSTNode<K, V> r) {
        key = k;
        val = v;
        left = l;
        right = r;
    }

    public static <K extends Comparable<K>, V> BSTNode<K, V> insert(BSTNode<K, V> node, K key, V val) {
        if (node == null)
            return new BSTNode<K, V>(key, val, null, null);

        if (key.compareTo(node.key) == 0) {
            node.val = val;
        } else if (key.compareTo(node.key) < 0) {
            node.left = insert(node.left, key, val);
        } else {
            node.right = insert(node.left, key, val);
        }

        return node;
    }

    public static <K extends Comparable<K>, V> V get(BSTNode<K, V> node, K k) {
        if (node == null) return null;

        if (k.compareTo(node.key) == 0) {
            return node.val;
        } else if (k.compareTo(node.key) < 0) {
            return get(node.left, k);
        } else {
            return get(node.right, k);
        }
    }

    public static <K extends Comparable<K>, V> BSTNode<K, V> remove(BSTNode<K, V> node, K k) {
        if (node == null) return null;

        if (k.compareTo(node.key) == 0) {
            if (node.left == null && node.right == null) {
                return null;
            } else if (node.left != null && node.right == null) {
                return node.left;
            } else if (node.left == null && node.right != null) {
                return node.right;
            } else {
                node.right = swapSmallest(node.right, node);
            }
        } else if (k.compareTo(node.key) < 0) {
            node.left = remove(node.left, k);
        } else {
            node.right = remove(node.right, k);
        }

        return node;
    }

    public static <K extends Comparable<K>, V> BSTNode<K, V> swapSmallest(BSTNode<K, V> T, BSTNode<K, V> R) {
        if (T.left == null) {
            R.val = T.val;
            return T.right;
        } else {
            T.left = swapSmallest(T.left, R);
            return T;
        }
    }

}

