class TestSortedComparableList {
    public static void main(String[] args) {
        int[] nums1 = {8, 2, 5, 9, 3};
        int[] nums2 = {7, 6, 4, 5, 3};

        SortedComparableList s1 = SortedComparableList.list(nums1);
        SortedComparableList s2 = SortedComparableList.list(nums2);

        System.out.println("s1: " + s1);
        System.out.println("s2: " + s2);

        s1.extend(s2);
        System.out.println("After s1.extend(s).");
        System.out.println("s1:" + s1);
        System.out.println("s2:" + s2);

        s1.squish();
        System.out.println("After s1.squish():" + s1);

        s1.twin();
        System.out.println("After s1.twin():" + s1);

    }

}
