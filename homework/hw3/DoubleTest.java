class DoubleTest {
    public static void main(String[] args) {
        double d = 1.0e-10;
        if (d == 0) {
            System.out.println(d + " == 0");
        } else if (d > 0) {
            System.out.println(d + " > 0");
        }

        System.out.println("(int)" + d + " : " + (int)d);
    }
}


