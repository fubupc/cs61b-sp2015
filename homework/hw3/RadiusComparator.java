import java.util.Comparator;

/**
 * MassComparator.java
 */

public class RadiusComparator implements Comparator<Planet> {

    public RadiusComparator() {
    }

    /** Returns the difference in mass as an int.
     *  Round after calculating the difference. */
    public int compare(Planet planet1, Planet planet2) {
        double diff = planet1.getRadius() - planet2.getRadius();

        if (diff < 0) {
           return -1;
        } else if (diff == 0) {
           return 0;
        } else {
           return 1;
        } 
    }
}
