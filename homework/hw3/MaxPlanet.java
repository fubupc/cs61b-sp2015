import java.util.Comparator;
/* Import anything else you need here. */

/**
 * MaxPlanet.java
 */

public class MaxPlanet {

    /** Returns the Planet with the maximum value according to Comparator c. */
    public static Planet maxPlanet(Planet[] planets, Comparator<Planet> c) {
        if (planets.length == 0) return null;

        Planet max = planets[0];

        for (int i = 1; i < planets.length; i++) {
            if (c.compare(max, planets[i]) < 0) {
                max = planets[i];
            }
        }

        return max;
    }

    public static void main(String[] args) {
        Planet[] planets = {
            new Planet(1.4960e+11, 0.0000e+00, 0.0000e+00, 2.9800e+04, 5.9740e+24, 0.00, "earth.gif"),
            new Planet(2.2790e+11, 0.0000e+00, 0.0000e+00, 2.4100e+04, 6.4190e+23, 0.01, "mars.gif"),
            new Planet(5.7900e+10, 0.0000e+00, 0.0000e+00, 4.7900e+04, 3.3020e+23, 0.02, "mercury.gif"),
        };

        System.out.println("max mass planet: " + maxPlanet(planets, new MassComparator()).img);
        System.out.println("max radius planet: " + maxPlanet(planets, new RadiusComparator()).img);
    }
}
