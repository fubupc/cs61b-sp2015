class CalculatorUI {
    public static void main(String[] args) {
        Calculator calc = new Calculator();

        while (true) {
            System.out.print("> ");

            String command = StdIn.readString();

            switch (command) {

                case "quit":
                    return;
                case "history": 
                    int num = StdIn.readInt();
                    calc.printHistory(num);
                    break;
                case "dump":
                    calc.printAllHistory();
                    break;
                case "undo":
                    calc.undoEquation();
                    break;
                case "clear":
                    calc.clearHistory();
                    break;
                case "sum":
                    System.out.println(calc.cumulativeSum());
                    break;
                case "product":
                    System.out.println(calc.cumulativeProduct());
                    break;
                default:
                    int a = Integer.parseInt(command);
                    String op = StdIn.readString();
                    int b = StdIn.readInt();
                    int result = 0;
                    
                    if (op.equals("+")) {
                        result = calc.add(a, b);
                    } else if (op.equals("*")) {
                        result = calc.multiply(a, b);
                    } else {
                        System.out.println("unknow operator: " + op);
                        break;
                    }

                    calc.saveEquation(a + " " + op + " " + b, result);
                    System.out.println(result);
            }
        }
    }
}
