class Test {
    public static void main(String[] args) {
        int i = pow(2, 30);

        System.out.println("2 ^ 30: " + i);
        System.out.println("(2 ^ 30) * 2: " + i * 2);
        System.out.println("(2 ^ 30) << 1: " + (i << 1));

        int min = -2147483648;
        System.out.println("min: " + min);
        System.out.println("-min: " + (-min));

        System.out.println("3 * -2147483647 should be -2147483645: " + (3 * -2147483647));
    }

    public static int pow(int base, int n) {
        if (n == 0) {
            return 1;
        } else {
            return base * pow(base, n - 1);
        }
    }

}
