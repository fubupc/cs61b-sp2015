import list.EquationList;

public class Calculator {
    // YOU MAY WISH TO ADD SOME FIELDS

    private EquationList historyHead = null;
    private int historySize = 0;

    /**
     * TASK 2: ADDING WITH BIT OPERATIONS
     * add() is a method which computes the sum of two integers x and y using 
     * only bitwise operators.
     * @param x is an integer which is one of two addends
     * @param y is an integer which is the other of the two addends
     * @return the sum of x and y
     **/
    public int add(int x, int y) {
        // YOUR CODE HERE
        int carry = 0;
        int sum = 0;

        for (int i = 0; i < 32; i++) {
            int b1 = ((x >>> i) << 31) >>> 31;
            int b2 = ((y >>> i) << 31) >>> 31;

            int[] r = fullAdder(b1, b2, carry);

            sum = sum | r[0] << i;
            carry = r[1];
        }

        return sum;
    }

    /**
     * half adder.
     * input: two bit b1 and b2
     * output: two bit array: a[0] is sum, a[1] is carry.
     */
    public int[] halfAdder(int b1, int b2) {
        int[] a = new int[2];

        a[0] = b1 ^ b2;
        
        if (b1 == 1 && b2 == 1) {
            a[1] = 1;
        }

        return a;
    }

    public int[] fullAdder(int b1, int b2, int carry) {
        int[] s1 = halfAdder(b1, b2);
        int[] s2 = halfAdder(s1[0], carry);

        int[] finalResult = new int[2];

        finalResult[0] = s2[0];
        finalResult[1] = s1[1] | s2[1];

        return finalResult;
    }

    /**
     * TASK 3: MULTIPLYING WITH BIT OPERATIONS
     * multiply() is a method which computes the product of two integers x and 
     * y using only bitwise operators.
     * @param x is an integer which is one of the two numbers to multiply
     * @param y is an integer which is the other of the two numbers to multiply
     * @return the product of x and y
     **/
    public int multiply(int x, int y) {
        // YOUR CODE HERE
        int sum = 0;

        for (int exp = 0; exp < 32; exp++) {
            int digit = ((y >>> exp) << 31) >>> 31;
            if (digit == 1) {
                sum = add(sum, x << exp);
            }
        }

        return sum;
    }

    /**
     * TASK 5A: CALCULATOR HISTORY - IMPLEMENTING THE HISTORY DATA STRUCTURE
     * saveEquation() updates calculator history by storing the equation and 
     * the corresponding result.
     * Note: You only need to save equations, not other commands.  See spec for 
     * details.
     * @param equation is a String representation of the equation, ex. "1 + 2"
     * @param result is an integer corresponding to the result of the equation
     **/
    public void saveEquation(String equation, int result) {
        // YOUR CODE HERE

        EquationList newItem = new EquationList(equation, result, null);

        if (historyHead == null) {
            historyHead = newItem;
        } else {
            newItem.next = historyHead;
            historyHead = newItem;
        }

        historySize++;
    }

    /**
     * TASK 5B: CALCULATOR HISTORY - PRINT HISTORY HELPER METHODS
     * printAllHistory() prints each equation (and its corresponding result), 
     * most recent equation first with one equation per line.  Please print in 
     * the following format:
     * Ex   "1 + 2 = 3"
     **/
    public void printAllHistory() {
        // YOUR CODE HERE
        printHistory(historySize);
    }

    /**
     * TASK 5B: CALCULATOR HISTORY - PRINT HISTORY HELPER METHODS
     * printHistory() prints each equation (and its corresponding result), 
     * most recent equation first with one equation per line.  A maximum of n 
     * equations should be printed out.  Please print in the following format:
     * Ex   "1 + 2 = 3"
     **/
    public void printHistory(int n) {
        // YOUR CODE HERE
        EquationList ptr = historyHead;

        while (ptr != null && n > 0) {
            System.out.println(ptr.equation + " = " + ptr.result);
            ptr = ptr.next;
            n--;
        }
    }    

    /**
     * TASK 6: CLEAR AND UNDO
     * undoEquation() removes the most recent equation we saved to our history.
    **/
    public void undoEquation() {
        if (historySize >= 1) {
            historyHead = historyHead.next;
            historySize--;
        }
    }

    /**
     * TASK 6: CLEAR AND UNDO
     * clearHistory() removes all entries in our history.
     **/
    public void clearHistory() {
        // YOUR CODE HERE
        historyHead = null;
        historySize = 0;
    }

    /**
     * TASK 7: ADVANCED CALCULATOR HISTORY COMMANDS
     * cumulativeSum() computes the sum over the result of each equation in our 
     * history.
     * @return the sum of all of the results in history
     **/
    public int cumulativeSum() {
        // YOUR CODE HERE
        int sum = 0;
        EquationList ptr = historyHead;

        while (ptr != null) {
            sum = add(sum, ptr.result);
            ptr = ptr.next;
        }

        return sum;
    }

    /**
     * TASK 7: ADVANCED CALCULATOR HISTORY COMMANDS
     * cumulativeProduct() computes the product over the result of each equation 
     * in history.
     * @return the product of all of the results in history
     **/
    public int cumulativeProduct() {
        // YOUR CODE HERE
        int product = 1;
        EquationList ptr = historyHead;

        while (ptr != null) {
            product = multiply(product, ptr.result);
            ptr = ptr.next;
        }

        return product;
    }
}
