import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {
    /* Do not change this to be private. For silly testing reasons it is public. */
    public Calculator tester;

    /**
     * setUp() performs setup work for your Calculator.  In short, we 
     * initialize the appropriate Calculator for you to work with.
     * By default, we have set up the Staff Calculator for you to test your 
     * tests.  To use your unit tests for your own implementation, comment 
     * out the StaffCalculator() line and uncomment the Calculator() line.
     **/
    @Before
    public void setUp() {
        //tester = new StaffCalculator(); // Comment me out to test your Calculator
        tester = new Calculator();   // Un-comment me to test your Calculator
    }

    // TASK 1: WRITE JUNIT TESTS
    // YOUR CODE HERE
    @Test
    public void addTest() {

        assertEquals(1 + 5, tester.add(1, 5));
        assertEquals(-1 + 5, tester.add(-1, 5));
        assertEquals(-1 + -5, tester.add(-1, -5));
        assertEquals(0 + -5, tester.add(0, -5));
        assertEquals(0 + 5, tester.add(0, 5));

        assertEquals(-2147483647 + 2, tester.add(-2147483647, 2));
    }

    @Test
    public void multiplyTest() {
        int min = -2147483648;
        assertEquals(2 * min, tester.multiply(2, min));
        assertEquals(2 * min, tester.multiply(min, 2));

        int n1 = pow(2, 30);
        assertEquals(2 * n1, min);
        assertEquals(2 * n1, tester.multiply(n1, 2));
        assertEquals(2 * n1, tester.multiply(2, n1));

    }

    /* Run the unit tests in this file. */
    public static void main(String... args) {
        jh61b.junit.textui.runClasses(CalculatorTest.class);
    }       

    /* helper */
    public static int pow(int base, int n) {
        if (n == 0) {
            return 1;
        } else {
            return base * pow(base, n - 1);
        }
    }

}
