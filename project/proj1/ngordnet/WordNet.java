package ngordnet;

import edu.princeton.cs.introcs.In;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Arrays;

public class WordNet {
    private HashMap<Integer, int[]> synsetToHyponyms;
    private ArrayList<String[]> synsetWords;
    private HashMap<String, ArrayList<Integer>> wordToSynsets;

    public WordNet(String synsetFile, String hyponymFile) {
        synsetToHyponyms = new HashMap<Integer, int[]>();
        synsetWords = new ArrayList<String[]>();
        wordToSynsets = new HashMap<String, ArrayList<Integer>>();

        In synsetIn = new In(synsetFile);
        while (synsetIn.hasNextLine()) {
            String[] parts = synsetIn.readLine().split(",");
            int synId = Integer.parseInt(parts[0]);
            String[] words = parts[1].split(" ");

            addSynset(synId, words);

            for (String word : words) {
                addWord(word, synId);
            }
        }
        synsetIn.close();

        In hyponymIn = new In(hyponymFile);
        while (hyponymIn.hasNextLine()) {
            String[] parts = hyponymIn.readLine().split(",");
            int synId = Integer.parseInt(parts[0]);

            int[] hypIds = new int[parts.length - 1];

            for (int i = 0; i < hypIds.length; i++) {
                hypIds[i] = Integer.parseInt(parts[i + 1]);
            }

            addHyponyms(synId, hypIds);
        }
        hyponymIn.close();

    }

    private void addWord(String word, int synId) {
        ArrayList<Integer> ids = wordToSynsets.get(word);

        if (ids == null) {
            ids = new ArrayList<Integer>();
        }

        ids.add(synId);
        wordToSynsets.put(word, ids);
    }

    private void addSynset(int synId, String[] words) {
        synsetWords.add(synId, words);
    }

    private void addHyponyms(int synId, int[] hyponymIds) {
        synsetToHyponyms.put(synId, hyponymIds);
    }

    public boolean isNoun(String word) {
        return wordToSynsets.containsKey(word);
    }

    public Set<String> nouns() {
        return wordToSynsets.keySet();
    }

    public Set<String> hyponyms(String word) {
        Set<String> result = new HashSet<String>();

        ArrayList<Integer> synIds = wordToSynsets.get(word);

        if (synIds == null) return result;

        for (int synId : synIds) {
            result.addAll(hyponymHelper(synId));
        }

        return result;
    }

    public Set<String> hyponymHelper(int synId) {
        Set<String> result = new HashSet<String>();

        result.addAll(Arrays.asList(synsetWords.get(synId)));

        int[] hypIds = synsetToHyponyms.get(synId);
        if (hypIds != null) {
            for (int hypId : hypIds) {
                result.addAll(hyponymHelper(hypId));
            }
        }

        return result;
    }
}
