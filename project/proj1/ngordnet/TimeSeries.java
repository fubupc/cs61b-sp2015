package ngordnet;

import java.util.Collection;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.ArrayList;
import java.util.Set;
import java.util.Map;


public class TimeSeries<T extends Number> extends TreeMap<Integer, T> {
    public TimeSeries() {}

    public TimeSeries(TimeSeries<T> ts) {
        for (Map.Entry<Integer, T> e : ts.entrySet()) {
            this.put(e.getKey(), e.getValue());
        }
    }

    public TimeSeries(TimeSeries<T> ts, int startYear, int endYear) {
        for (Map.Entry<Integer, T> e : ts.entrySet()) {
            if (startYear <= e.getKey() && e.getKey() <= endYear) {
                this.put(e.getKey(), e.getValue());
            }
        }
    }

    public Collection<Number> years() {
        Collection<Number> ret = new TreeSet<Number>();
        ret.addAll(super.keySet());
        return ret;
    }

    public Collection<Number> data() {
        Collection<Number> ret = new ArrayList<Number>();
        ret.addAll(super.values());
        return ret;  // same order as years.
    }

    public TimeSeries<Double> plus(TimeSeries<? extends Number> ts) {
        TimeSeries<Double> ret = new TimeSeries<Double>();

        for (int year: this.keySet()) {
            ret.put(year, this.get(year).doubleValue());
        }

        for (int year : ts.keySet()) {
            Number myVal = this.get(year);
            Number otherVal = ts.get(year);

            if (myVal != null) {
                ret.put(year, myVal.doubleValue() + otherVal.doubleValue());
            } else {
                ret.put(year, otherVal.doubleValue());
            }
        }

        return ret;
    }

    public TimeSeries<Double> dividedBy(TimeSeries<? extends Number> ts) {
        TimeSeries<Double> ret = new TimeSeries<Double>();

        for (int year: this.keySet()) {
            if (ts.get(year) == null) {
                throw new IllegalArgumentException();
            }
        }

        for (int year : ts.keySet()) {
            Number myVal = this.get(year);
            Number otherVal = ts.get(year);

            if (myVal != null) {
                ret.put(year, myVal.doubleValue() / otherVal.doubleValue());
            } else {
                ret.put(year, 0.0);
            }
        }

        return ret;

    }

}
