package ngordnet;

import java.util.*;


public class YearlyRecord {
    private Map<String, Integer> wordToCount;
    private Map<Integer, List<String>> countToWords;
    private Map<String, Integer> wordToRank;

    private boolean cacheValid;

    public YearlyRecord() {
        wordToCount = new TreeMap<String, Integer>();
        countToWords = new TreeMap<Integer, List<String>>();
        wordToRank = new TreeMap<String, Integer>();

        cacheValid = true;
    }

    public YearlyRecord(Map<String, Integer> rawData) {
        this();

        for (String word : rawData.keySet()) {
            put(word, rawData.get(word));
        }

        cacheValid = false;
    }

    public int size() {
        return wordToCount.size();
    }

    public int count(String word) {
        return wordToCount.get(word);
    }

    public Collection<String> words() {
        List<String> allWords = new ArrayList<String>();

        for (List<String> words : countToWords.values()) {
            allWords.addAll(words);
        }

        return allWords;
    }

    public Collection<Number> counts() {
        Collection<Number> ret = new ArrayList<Number>();

        for (Map.Entry<Integer, List<String>> e : countToWords.entrySet()) {
            int count = e.getKey();
            List<String> words = e.getValue();

            for (int i = 0; i < words.size(); i++) {
                ret.add(count);
            }
        }

        return ret;
    }

    public void put(String word, int count) {
        List<String> words;

        wordToCount.put(word, count);

        if (countToWords.containsKey(count)) {
            words = countToWords.get(count);
        } else {
            words = new ArrayList<String>();
        }

        words.add(word);
        countToWords.put(count, words);

        cacheValid = false;
    }

    public int rank(String word) {
        if (!cacheValid) {
            int currentRank = size();

            for (List<String> words : countToWords.values()) {
                for (int i = words.size() - 1; i >= 0; i--) {
                    wordToRank.put(words.get(i), currentRank);
                    currentRank--;
                }
            }

            cacheValid = true;
        }

        return wordToRank.get(word);
    }

}

