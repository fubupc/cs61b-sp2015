package ngordnet;

import java.util.TreeMap;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import edu.princeton.cs.introcs.In;


public class NGramMap {
    private Map<Integer, YearlyRecord> yearlyMap;
    private Map<String, TimeSeries<Integer>> wordMap;
    private TimeSeries<Long> totalCount;

    public NGramMap(String wordsFilename, String countsFilename) {
        yearlyMap = new TreeMap<Integer, YearlyRecord>();
        wordMap = new HashMap<String, TimeSeries<Integer>>();
        totalCount = new TimeSeries<Long>();

        In wordsIn = new In(wordsFilename);
        while (wordsIn.hasNextLine()) {
            String[] fields = wordsIn.readLine().split("\t");
            String word = fields[0];
            int year = Integer.parseInt(fields[1]);
            int count = Integer.parseInt(fields[2]);

            YearlyRecord record = yearlyMap.get(year);
            if (record == null) {
                record = new YearlyRecord();
                yearlyMap.put(year, record);
            }
            record.put(word, count);

            TimeSeries<Integer> series = wordMap.get(word);
            if (series == null) {
                series = new TimeSeries<Integer>();
                wordMap.put(word, series);
            }
            series.put(year, count);
        }
        wordsIn.close();

        In countsIn = new In(countsFilename);
        while (countsIn.hasNextLine()) {
            String[] fields = countsIn.readLine().split(",");
            int year = Integer.parseInt(fields[0]);
            Long count = Long.parseLong(fields[1]);
            totalCount.put(year, count);
        }
        countsIn.close();
    }

    public int countInYear(String word, int year) {
        YearlyRecord record = getRecord(year);

        if (record == null) {
            return 0;
        }

        Integer count = record.count(word);

        if (count == null) {
            count = 0;
        } 
        return count;
    }

    public YearlyRecord getRecord(int year) {
        return yearlyMap.get(year);
    }

    public TimeSeries<Long> totalCountHistory() {
        return totalCount;
    }

    public TimeSeries<Integer> countHistory(String word, int startYear, int endYear) {
        return new TimeSeries<Integer>(wordMap.get(word), startYear, endYear);
    }

    public TimeSeries<Integer> countHistory(String word) {
        return new TimeSeries<Integer>(wordMap.get(word));
    }

    public TimeSeries<Double> weightHistory(String word) {
        TimeSeries<Double> weight = new TimeSeries<Double>();

        TimeSeries<Integer> count = countHistory(word);
        TimeSeries<Long> total = totalCountHistory();

        for (int year : count.keySet()) {
            weight.put(year, (double) count.get(year) / total.get(year));
        }

        return weight;
    }

    public TimeSeries<Double> weightHistory(String word, int startYear, int endYear) {
        return new TimeSeries<Double>(weightHistory(word), startYear, endYear);
    }

    public TimeSeries<Double> summedWeightHistory(Collection<String> words) {
        TimeSeries<Double> summed = new TimeSeries<Double>();

        for (String word : words) {
            summed = summed.plus(weightHistory(word));
        }

        return summed;
    }

    public TimeSeries<Double> summedWeightHistory(Collection<String> words, int startYear, int endYear) {
        return new TimeSeries<Double>(summedWeightHistory(words), startYear, endYear);
    }

}
